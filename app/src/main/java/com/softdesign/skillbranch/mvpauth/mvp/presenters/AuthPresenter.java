package com.softdesign.skillbranch.mvpauth.mvp.presenters;


import android.support.annotation.Nullable;

import com.softdesign.skillbranch.mvpauth.mvp.models.AuthModel;
import com.softdesign.skillbranch.mvpauth.mvp.views.IAuthView;
import com.softdesign.skillbranch.mvpauth.ui.custom_views.AuthPanel;

/**
 * Created by Иван on 20.10.2016.
 */
public class AuthPresenter implements IAuthPresenter {
    private static AuthPresenter ourInstance = new AuthPresenter();

    private AuthModel mAuthModel;
    private IAuthView mAuthView;

    public AuthPresenter() {
        mAuthModel = new AuthModel();
    }

    public static AuthPresenter getInstance() {
        return ourInstance;
    }


    @Override
    public void takeView(IAuthView authView) {
        mAuthView = authView;
    }

    @Override
    public void dropView() {
        mAuthView = null;
    }

    @Override
    public void initView() {
        if (getView() != null) {
            if (checkUserAuth()) {
                getView().hideLoginBtn();
            } else {
                getView().showLoginBtn();
            }
        }
    }




    @Nullable
    @Override
    public IAuthView getView() {
        return mAuthView;
    }

    @Override
    public void clickOnLogin() {
        if (getView() != null && getView().getAuthPanel() != null) {
            if (getView().getAuthPanel().isIdle()) {
                getView().getAuthPanel().setCustomState(AuthPanel.LOGIN_STATE);
            } else {
                // TODO: 20.10.2016 auth user
                mAuthModel.loginUser(getView().getAuthPanel().getUserEmail(), getView().getAuthPanel().getUserPassword());
                if (mAuthModel.isAuthUser()) mAuthView.hideLoginBtn();
                else mAuthView.showLoginBtn();
//                getView().showLoad();
//                getView().hideLoad();
                getView().showMessage("request for user auth");
            }
        }
    }


    @Override
    public void clickOnVk() {
        if (getView() != null)
            getView().showMessage("clickOnVk");

    }

    @Override
    public void clickOnFb() {
        if (getView() != null)
            getView().showMessage("clickOnFb");

    }

    @Override
    public void clickOnTwitter() {
        if (getView() != null)
            getView().showMessage("clickOnTwitter");
    }

    @Override
    public void clickOnShowCatalog() {
        if (getView() != null)
            getView().showMessage("Показать каталог");

    }


    @Override
    public boolean checkUserAuth() {
        return mAuthModel.isAuthUser();
    }

    @Override
    public boolean validateEmail(String email) {
        String pattern = "^[\\w\\+\\.\\%\\-]{3,256}\\@[a-zA-Z0-9][a-zA-Z0-9\\-]{1,64}(\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{1,25})[ ]*";
        return email.matches(pattern);
    }

    @Override
    public boolean validatePassword(String password) {
        String pattern = "[\\w]{8,}";
        return password.matches(pattern);
    }
}
