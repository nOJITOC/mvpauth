package com.softdesign.skillbranch.mvpauth.mvp.models;

import com.softdesign.skillbranch.mvpauth.data.managers.DataManager;

/**
 * Created by Иван on 20.10.2016.
 */
public class AuthModel {


    DataManager mDataManager;

    public AuthModel() {
        mDataManager = DataManager.getInstance();
    }



    public boolean isAuthUser() {
        return mDataManager.getPreferencesManager().isAuthToken();
    }

    public void loginUser(String login, String password) {
        // TODO: 20.10.2016 send data to server for auth
        mDataManager.userAuth(login, password);

    }
}
