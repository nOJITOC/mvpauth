package com.softdesign.skillbranch.mvpauth.ui.activities;

import android.app.ProgressDialog;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;

import com.softdesign.skillbranch.mvpauth.BuildConfig;
import com.softdesign.skillbranch.mvpauth.R;
import com.softdesign.skillbranch.mvpauth.data.managers.ConstantManager;
import com.softdesign.skillbranch.mvpauth.mvp.presenters.AuthPresenter;
import com.softdesign.skillbranch.mvpauth.mvp.presenters.IAuthPresenter;
import com.softdesign.skillbranch.mvpauth.mvp.views.IAuthView;
import com.softdesign.skillbranch.mvpauth.ui.custom_views.AuthPanel;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

public class RootActivity extends AppCompatActivity implements IAuthView, View.OnClickListener {

    AuthPresenter mPresenter = AuthPresenter.getInstance();

    @BindView(R.id.coordinator_container)
    CoordinatorLayout mCoordinatorLayout;

    @BindView(R.id.auth_wrapper)
    AuthPanel mAuthPanel;

    @BindView(R.id.login_btn)
    Button mLoginBtn;
    @BindView(R.id.show_catalog_btn)
    Button mShowCatalogBtn;
    ProgressDialog mProgressDialog;

    //region ========================Life cicle===============================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);
        ButterKnife.bind(this);
        mPresenter.takeView(this);
        mPresenter.initView();

        mLoginBtn.setOnClickListener(this);
        mShowCatalogBtn.setOnClickListener(this);

    }

    @Override
    protected void onDestroy() {
        mPresenter.dropView();
        super.onDestroy();
    }
    //endregion

    //region =======================IAuthView================================
    @Override
    public AuthPanel getAuthPanel() {
        return mAuthPanel;
    }

    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable t) {
        if (BuildConfig.DEBUG) {
            showMessage(t.getMessage());
            t.printStackTrace();
        } else {
            showMessage(getString(R.string.sorry_error));
            // TODO: 20.10.2016 send error stacktrace to crashliticks
        }
    }

    @Override
    public void showLoad() {
        this.mProgressDialog = ProgressDialog.show(this, null, getString(R.string.progress_dialog_text), true, false);

    }

    @Override
    public void hideLoad() {
        if (mProgressDialog != null) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mProgressDialog.hide();
                }
            }, ConstantManager.PD_DELAY);
        }
    }

    @Override
    public IAuthPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void showLoginBtn() {
        mLoginBtn.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoginBtn() {
        mLoginBtn.setVisibility(View.GONE);
    }

// endregion


    @Override
    public void onBackPressed() {
        if (!mAuthPanel.isIdle()) {
            mAuthPanel.setCustomState(AuthPanel.IDLE_STATE);
        } else
            super.onBackPressed();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.show_catalog_btn:
                mPresenter.clickOnShowCatalog();

                break;
            case R.id.login_btn:
                mPresenter.clickOnLogin();
                break;
        }
    }

}
