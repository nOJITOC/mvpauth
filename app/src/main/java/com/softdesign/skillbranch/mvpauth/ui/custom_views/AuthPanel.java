package com.softdesign.skillbranch.mvpauth.ui.custom_views;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.softdesign.skillbranch.mvpauth.R;
import com.softdesign.skillbranch.mvpauth.mvp.presenters.AuthPresenter;
import com.softdesign.skillbranch.mvpauth.utils.validators.EmailValidator;
import com.softdesign.skillbranch.mvpauth.utils.validators.PasswordValidator;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AuthPanel extends LinearLayout {
    private static final long DURATION = 500;
    private static String TAG = "AuthPanel";
    public static final int LOGIN_STATE = 0;
    public static final int IDLE_STATE = 1;
    private int mCustomState = 1;

    @BindView(R.id.auth_card)
    CardView mAuthCard;
    @BindView(R.id.login_email_et)
    EditText mEmailEt;
    @BindView(R.id.login_password_et)
    EditText mPasswordEt;
    @BindView(R.id.login_email_wrap)
    TextInputLayout mEmailWrap;
    @BindView(R.id.login_password_wrap)
    TextInputLayout mPasswordlWrap;
    @BindView(R.id.login_btn)
    Button mLoginBtn;
    @BindView(R.id.show_catalog_btn)
    Button mShowCatalogBtn;
    AuthPresenter mPresenter;


    public AuthPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    // TODO: 20.10.2016 validate and save state for email input
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mPresenter = AuthPresenter.getInstance();
        ButterKnife.bind(this);
        mPasswordEt.addTextChangedListener(new PasswordValidator(this, mPasswordlWrap,getContext().getString(R.string.password_error_text)));
        mEmailEt.addTextChangedListener(new EmailValidator(this,mEmailWrap,getContext().getString(R.string.email_error_text)));

        showViewFromState();
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        SavedState savedState = new SavedState(superState);
        savedState.state = mCustomState;
        savedState.login = mEmailEt.getText().toString();
        savedState.password = mPasswordEt.getText().toString();

        return savedState;
    }

    public String getUserEmail() {
        return String.valueOf(mEmailEt.getText());
    }

    public String getUserPassword() {
        return String.valueOf(mPasswordEt.getText());
    }



    public boolean validateEmail(String email){
        return mPresenter.validateEmail(email);
    }
    public boolean validatePassword(String password){
        return mPresenter.validatePassword(password);
    }

    public boolean isIdle() {
        return mCustomState == IDLE_STATE;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        SavedState savedState = (SavedState) state;
        setCustomState(savedState.state);
        mEmailEt.setText(savedState.login);
        mPasswordEt.setText(savedState.password);
        super.onRestoreInstanceState(savedState.getSuperState());

    }

    public void setCustomState(int state) {
        mCustomState = state;
        showViewFromState();
    }

    private void showLoginState() {

        mAuthCard.setVisibility(VISIBLE);
        Animation animation = new ScaleAnimation(
                1.0f, 1.0f,
                0.3f, 1.0f,
                Animation.RELATIVE_TO_SELF, 1.0f,
                Animation.RELATIVE_TO_SELF, 1.0f
        );


        mAuthCard.clearAnimation();
        animation.setDuration(DURATION);
        mAuthCard.setAnimation(animation);
        mShowCatalogBtn.setVisibility(GONE);

    }

    private void showIdleState() {

        Animation animation = new ScaleAnimation(
                1.0f, 1.0f,
                1.0f,0.3f,
//                mAuthCard.getMeasuredHeight(), mShowCatalogBtn.getMeasuredHeight(),
                Animation.RELATIVE_TO_SELF, 1.0f,
                Animation.RELATIVE_TO_SELF, 1.0f
        );
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mShowCatalogBtn.setVisibility(VISIBLE);
                mAuthCard.setVisibility(GONE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        animation.setDuration(DURATION);
        mAuthCard.clearAnimation();
        mAuthCard.startAnimation(animation);




    }


    private void showViewFromState() {
        if (mCustomState == LOGIN_STATE) {
            showLoginState();
        } else {
            showIdleState();
        }

    }


    static class SavedState extends BaseSavedState {

        private int state;
        private String login;
        private String password;

        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            @Override
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            @Override
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };

        public SavedState(Parcelable superState) {
            super(superState);
        }

        private SavedState(Parcel source) {
            super(source);
            state = source.readInt();
            login = source.readString();
            password = source.readString();
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeInt(state);
            out.writeString(login);
            out.writeString(password);
        }
    }
}
