package com.softdesign.skillbranch.mvpauth.utils.validators;

import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.widget.EditText;

import com.softdesign.skillbranch.mvpauth.ui.custom_views.AuthPanel;

/**
 * Created by Иван on 22.10.2016.
 */
public abstract class BaseValidator implements IValidator {
    protected TextInputLayout mTextInputLayout;

    private String error;

    protected AuthPanel mPanel;

    private boolean isValid;


    public BaseValidator(AuthPanel panel, TextInputLayout textInputLayout, String error) {
        mPanel = panel;
        mTextInputLayout = textInputLayout;
        this.error = error;
    }

    @Override
    public boolean isValid() {
        return isValid;
    }

    @Override
    public void setValid(boolean cond) {
        this.isValid = cond;
    }

    public String getError() {
        return isValid() ? "" : error;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }


}
