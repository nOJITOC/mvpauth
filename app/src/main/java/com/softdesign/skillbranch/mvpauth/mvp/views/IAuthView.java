package com.softdesign.skillbranch.mvpauth.mvp.views;

import android.support.annotation.Nullable;

import com.softdesign.skillbranch.mvpauth.mvp.presenters.IAuthPresenter;
import com.softdesign.skillbranch.mvpauth.ui.custom_views.AuthPanel;

/**
 * Created by Иван on 20.10.2016.
 */
public interface IAuthView {
    void showMessage(String message);
    void showError(Throwable t);

    void showLoad();
    void hideLoad();

    IAuthPresenter getPresenter();

    void showLoginBtn();
    void hideLoginBtn();

//    void testShowLoginCard();
    @Nullable
    AuthPanel getAuthPanel();
}
