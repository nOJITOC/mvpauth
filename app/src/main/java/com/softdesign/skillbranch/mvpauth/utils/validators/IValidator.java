package com.softdesign.skillbranch.mvpauth.utils.validators;

import android.text.TextWatcher;

/**
 * Created by Иван on 21.10.2016.
 */
public interface IValidator extends TextWatcher{
    public boolean isValid();
    public void setValid(boolean cond);
}
