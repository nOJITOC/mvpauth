package com.softdesign.skillbranch.mvpauth.data.managers;

import android.content.SharedPreferences;


import com.softdesign.skillbranch.mvpauth.utils.MvpAuthApplication;

/**
 * Created by Иван on 15.10.2016.
 */
public class PreferencesManager {
    private SharedPreferences mSharedPreferences;
    public PreferencesManager(){
        mSharedPreferences = MvpAuthApplication.getSharedPreferences();
    }

    public void saveAuthToken(String token){
        SharedPreferences.Editor editor=mSharedPreferences.edit();
        editor.putString(ConstantManager.AUTH_TOKEN,token);
        editor.apply();
    }



    public String getAuthToken(){
        return mSharedPreferences.getString(ConstantManager.AUTH_TOKEN,null);
    }
    public boolean isAuthToken(){
        return getAuthToken()!=null;
    }




}
