package com.softdesign.skillbranch.mvpauth.data.managers;

/**
 * Created by Иван on 21.10.2016.
 */
public interface ConstantManager {
    String AUTH_TOKEN = "AUTH_TOKEN";
    long PD_DELAY = 1000;
}
