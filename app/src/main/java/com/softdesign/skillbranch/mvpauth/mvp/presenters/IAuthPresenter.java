package com.softdesign.skillbranch.mvpauth.mvp.presenters;

import android.support.annotation.Nullable;

import com.softdesign.skillbranch.mvpauth.mvp.views.IAuthView;

/**
 * Created by Иван on 20.10.2016.
 */
public interface IAuthPresenter {

    void takeView(IAuthView authView);
    void dropView();
    void initView();

    @Nullable
    IAuthView getView();

    void clickOnLogin();
    void clickOnVk();
    void clickOnFb();
    void clickOnTwitter();
    void clickOnShowCatalog();


    boolean checkUserAuth();
    boolean validateEmail(String email);
    boolean validatePassword(String password);


}
