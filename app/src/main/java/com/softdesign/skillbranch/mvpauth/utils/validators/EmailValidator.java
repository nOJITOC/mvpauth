package com.softdesign.skillbranch.mvpauth.utils.validators;

import android.print.PrintAttributes;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.widget.EditText;

import com.softdesign.skillbranch.mvpauth.ui.custom_views.AuthPanel;

/**
 * Created by Иван on 21.10.2016.
 */
public class EmailValidator extends BaseValidator {


    public EmailValidator(AuthPanel panel, TextInputLayout textInputLayout, String error) {
        super(panel, textInputLayout, error);
    }

    @Override
    public void afterTextChanged(Editable editable) {
        setValid(mPanel.validateEmail(editable.toString()));
        mTextInputLayout.setErrorEnabled(isValid());
        mTextInputLayout.setError(getError());
    }
}
