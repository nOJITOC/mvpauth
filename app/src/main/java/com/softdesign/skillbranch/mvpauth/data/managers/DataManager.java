package com.softdesign.skillbranch.mvpauth.data.managers;

import android.content.Context;

import com.softdesign.skillbranch.mvpauth.utils.MvpAuthApplication;

public class DataManager {

    private static final String TAG = "DataManager";
    private static DataManager INSTANCE = null;
    private PreferencesManager mPreferencesManager;

    private Context mContext;

    public DataManager() {
        this.mContext = MvpAuthApplication.getContext();
        mPreferencesManager = new PreferencesManager();
    }



    public PreferencesManager getPreferencesManager() {
        return mPreferencesManager;
    }


    public static DataManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DataManager();
        }
        return INSTANCE;

    }

    public Context getContex() {
        return mContext;
    }
    //region =======================Network================================
    public void userAuth(String login, String password){
        String token = null;
        // TODO: 21.10.2016  add request to server for user token
        mPreferencesManager.saveAuthToken(token);

    }
    //endregion
}

