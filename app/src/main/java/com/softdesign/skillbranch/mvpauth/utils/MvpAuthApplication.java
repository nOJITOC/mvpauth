package com.softdesign.skillbranch.mvpauth.utils;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class MvpAuthApplication extends Application {
    public static SharedPreferences sSharedPreferences;
    private static Context sContext;

    public static Context getContext() {
        return sContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        sContext = this;

    }


    public static SharedPreferences getSharedPreferences() {
        return sSharedPreferences;
    }
}